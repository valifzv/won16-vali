package org.fasttrackit;

public class TransitionClass {
    public static void main(String[] args) {
        System.out.println("- = Demo Shop");
        System.out.println("1. user can login with valid credentials.");
        Page page = new Page();
        page.openHomePage();
        Header header = new Header();
        header.clickOnTheLoginButton();
        ModalDialog modal = new ModalDialog();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("dino");
        System.out.println("Hi dino is display in heather: " + isLogged);
        System.out.println("Greeting mdg is: " + greetingsMessage);

        System.out.println("-------------------------------");
        System.out.println("2. User can add product to cart from product cards.");
        page.openHomePage();
        ProductCards cards = new ProductCards();
 //       Product awesomeGraniteChips = cards.getProductByName("Awesome Granite Chips");
 //       System.out.println("Product is: " + awesomeGraniteChips.getTitle());
 //       awesomeGraniteChips.clickOnTheProductCartIcon();

        System.out.println("-------------------------------");
        System.out.println("3. User can navigate to Home Page from Wishlist page.");
        page.openHomePage();
        header.clickOnTheWishlistIcon();
        String pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the wishlist page. Title is: "  + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the home page" + pageTitle);

        System.out.println("-------------------------------");
        System.out.println("4. User can navigate to Home Page from Cart page.");
        page.openHomePage();
        header.clickOnTheCartIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Cart page. Title is: " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the home page. Title is:  " + pageTitle);
    }
}
//user can navigate to home page from wishlist page
//1. open home page.
//2. Click on the favorites icon.
//3. Click on the Shopping bag icon.
//4. Expected result: Home Page is loaded

// - add product to cart from product cards
// 1. open home page.
// 2. click on the "Awesome Granite Chips" cart icon.
// 3. Expected result: mini cart icon shows 1 product in cart.

// 1. Open Home page.
// 2. Click on the Login button.
// 3. Click on the Username Field.
// 4. Type in dino.
// 5. click on the Password field.
// 6. type in choochoo.
// 7. click on the login button.
// Expected: Hi dino.