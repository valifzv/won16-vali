package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class ModalDialog {

    private final SelenideElement userName = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");
    private final SelenideElement logoutButton = $(".fa-sign-out-alt");
    private final SelenideElement errorMessage = $(".error");
    private final SelenideElement closeModal = $(".close");

    @Step("Type in username")
    public void typeInUsername(String user) {
        System.out.println("click on the username field.");
        userName.click();
        System.out.println("Type in " + user);
        userName.type(user);
    }

    @Step("Type in password")
    public void typeInPassword(String pass) {
        System.out.println("Click on the password field.");
        password.click();
        System.out.println("Type in " + pass);
        password.type(pass);
    }


    @Step("Click on login button")
    public void clickOnTheLoginButton() {
        System.out.println("Click on the login button.");
        loginButton.click();
    }

    @Step("Click on login button")
    public void clickOnTheLogoutButton() {
        System.out.println("Click on the logout button.");
        logoutButton.click();
    }

    public boolean isErrorMessageVisible() {
        return errorMessage.isDisplayed();
    }

    public void clickToCloseModal() {
        closeModal.click();
    }
}
