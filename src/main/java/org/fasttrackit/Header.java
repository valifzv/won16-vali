package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement greetingElement = $(".navbar-text span span");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final ElementsCollection shoppingCartBadges = $$(".shopping_cart_badge");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");
    private final SelenideElement searchField = $("#input-search");
    private final SelenideElement searchButton = $(".btn-light");
    private final SelenideElement brokenHeartButton = $(".fa-heart-broken");

    @Step()
    public void clickOnTheLoginButton() {
        loginButton.click();
        System.out.println("Click on the Login button.");
    }

    public void clickOnTheSearchField() {
        searchField.click();
        System.out.println("Click on the search field.");
    }

    @Step("Click on search button")
    public void clickOnTheSearchButton() {
        System.out.println("Click on the search button.");
        searchButton.click();
    }

    public void typeInProductName(String productId) {
        System.out.println("click on the search field.");
        searchField.click();
        System.out.println("Type in Awesome Granite Chips");
    }

    public String getGreetingsMessage() {
        return greetingElement.text();
    }

    public void clickOnTheWishlistIcon() {
        System.out.println("Click on the wishlist button.");
        wishlistButton.click();
    }

    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping Bag Icon");
        homePageButton.click();
    }

    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart Icon");
        cartIcon.click();
    }

    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }

    public boolean isShoppingBadgeVisible() {
        return !this.shoppingCartBadges.isEmpty();
    }

    public String getWishlistBadgeValue() {
        return this.shoppingCartBadge.text();
    }

    public void clickOnTheBrokenHeartButton() {
        System.out.println("Click on broken heart");
        brokenHeartButton.click();
    }
}
