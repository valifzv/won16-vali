package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {

    private final SelenideElement card;
    private final SelenideElement addToCartButton;
    private final String title;
    private final String price;
    private final SelenideElement addToWishlistButton;

    public Product(SelenideElement productTitle) {
        this.card = productTitle;
        this.addToCartButton = card.$(".card-footer .fa-cart-plus");
        this.addToWishlistButton = card.$(".card-footer .fa-heart");
        this.title = this.card.$(".card-link").text();
        this.price = this.card.$(".card-footer .card-text span").text();
    }

    public Product(String productId) {
        String productIdSelector = String.format("[href='#/product/%s']", productId);
        SelenideElement cardLink = $(productIdSelector);
       this.card = cardLink.parent().parent();
       this.addToCartButton = card.$(".card-footer .fa-cart-plus");
       this.addToWishlistButton = card.$(".card-footer .fa-heart");
       this.title = cardLink.text();
       this.price = this.card.$(".card-footer .card-text span").text();
    }

    public String getTitle() {
        return title;
    }

    public void clickOnTheProductCartIcon() {
        System.out.println("Click on the cart icon.");
        this.addToCartButton.click();
    }

    public String getPrice() {
        return price;
    }

    public void clickOnTheProductWishlistIcon() {
        System.out.println("Click on the wishlist icon.");
        this.addToWishlistButton.click();
    }
}
