package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private final SelenideElement emptyCartPageElement = $(".text-center");
    public final SelenideElement clickOnTheDeleteButton = $(".fa-trash");
    public final SelenideElement clickOnContinueShopping = $(".btn-danger");
    public final SelenideElement clickOnCheckoutButton = $(".btn-success");
    private final SelenideElement typeInFirstName = $("#first-name");
    private final SelenideElement typeInLastName = $("#last-name");
    private final SelenideElement typeInAddress = $("#address");
    public final SelenideElement clickOnContinueCheckoutButton = $(".btn-success");
    public final SelenideElement clickOnCancelButton = $(".btn-danger");
    public final SelenideElement clickOnCompleteYourOrder = $(".btn-success");

    public CartPage() {
    }

    public String getEmptyCartPageText() {
        return this.emptyCartPageElement.text();
    }

    public boolean isEmptyCartMessageDisplayed() {
        return emptyCartPageElement.isDisplayed();
    }

    public void typeInFirstName(String text) {
        System.out.println("click on the username field.");
        typeInFirstName.click();
        System.out.println("Type in " + text);
        typeInFirstName.type(text);
    }

    public void typeInLastName(String text) {
        System.out.println("click on the username field.");
        typeInLastName.click();
        System.out.println("Type in " + text);
        typeInLastName.type(text);
    }

    public void typeInAddress(String text) {
        System.out.println("click on the username field.");
        typeInAddress.click();
        System.out.println("Type in " + text);
        typeInAddress.type(text);
    }
}
