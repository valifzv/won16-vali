package org.fasttrackit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ProductSortingTest {
    Page page = new Page();
    ProductCards productList = new ProductCards();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickToReset();
    }

    @Test(description = "user can click on sort button")
    public void user_can_click_on_sort_button() {
        productList.clickOnTheSortButton();
        assertEquals(page.getPageTitle(), "Products", "Expected to be Sort by nma(A to Z)");
    }

    @Test(description = "when sorting products a to z products are sorted alphabetically")
    public void when_sorting_products_a_to_z_products_are_sorted_alphabetically() {
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheAZSortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());

    }

    @Test(description = "when sorting products Z to A products are sorted alphabetically DESC")
    public void when_sorting_products_Z_to_A_products_are_sorted_alphabetically_DESC() {
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheZASortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
    }

    @Test(description = "when sorting products by price low to high products are sorted by price low to high")
    public void when_sorting_products_by_price_low_to_high_products_are_sorted_by_price_low_to_high() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
    }

    @Test(description = "when sorting products by price high to low products are sorted by price high to low")
    public void when_sorting_products_by_price_high_to_low_products_are_sorted_by_price_high_to_low() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceHiLo();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
    }

    @Test(description = "user can type in the search field")
    public void user_can_type_in_the_search_field() {
        header.clickOnTheSearchField();
        assertEquals(page.getPageTitle(), "Products", "Expected to be Products page");
    }

    @Test(description = "user can search for a product")
    public void user_can_search_for_a_product() {
        header.clickOnTheSearchField();
        header.typeInProductName("Awesome Granite Chips");
        header.clickOnTheSearchButton();
        assertEquals(page.getPageTitle(), "Products", "Expected to be Awesome Granite Chips");
    }

    @Test(description = "user can search for a product")
    public void user_can_search_for_a_another_product() {
        header.clickOnTheSearchField();
        header.typeInProductName("Refined Frozen Mouse");
        header.clickOnTheSearchButton();
        assertEquals(page.getPageTitle(), "Products", "Expected to be Refined Frozen Mouse");
    }
}
