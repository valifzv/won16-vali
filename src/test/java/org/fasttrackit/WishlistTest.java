package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

//@Test(dependsOnGroups = "AuthenticationTest")
public class WishlistTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();
    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickToReset();
    }
    @Test(description = "user as a guest can navigate to Wishlist page")
    public void user_as_a_guest_can_navigate_to_Wishlist_page() {
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on the wishlist page");
    }

    @Test(description = "user dino can navigate to Wishlist page")
    public void user_dino_can_navigate_to_Wishlist_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on the wishlist page");
    }

    @Test(description = "user turtle can navigate to Wishlist page")
    public void user_turtle_can_navigate_to_Wishlist_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on the wishlist page");
    }

    @Test(description = "user beetle can navigate to Wishlist page")
    public void user_beetle_can_navigate_to_Wishlist_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on the wishlist page");
    }
    @Test(description = "user as a guest can navigate to Home Page from Wishlist page")
    public void user_as_a_guest_can_navigate_to_Home_Page_from_Wishlist_page() {
        header.clickOnTheWishlistIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page");
    }

    @Test(description = "user dino can navigate to Home Page from Wishlist page")
    public void user_dino_can_navigate_to_Home_Page_from_Wishlist_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheWishlistIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page");
    }

    @Test(description = "user turtle can navigate to Home Page from Wishlist page")
    public void user_turtle_can_navigate_to_Home_Page_from_Wishlist_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheWishlistIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page");
    }

    @Test(description = "user beetle can navigate to Home Page from Wishlist page")
    public void user_beetle_can_navigate_to_Home_Page_from_Wishlist_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheWishlistIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page");
    }

    @Test(description = "user as a guest can add products to wishlist from product cards")
    public void user_as_a_guest_can_add_Product_to_wishlist_from_product_cards() {
        Product product = new Product("8");
        product.clickOnTheProductWishlistIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getWishlistBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }
    @Test(description = "user can add product from Wishlist page to cart page")
    public void user_can_add_product_from_Wishlist_page_to_cart_page() {
        Product product = new Product("8");
        product.clickOnTheProductWishlistIcon();
        assertTrue(header.isShoppingBadgeVisible());
        header.clickOnTheWishlistIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
    }

    @Test(description = "user can remove products from Wishlist page")
    public void user_can_remove_products_from_Wishlist_page() {
        Product product = new Product("8");
        product.clickOnTheProductWishlistIcon();
        assertTrue(header.isShoppingBadgeVisible());
        header.clickOnTheWishlistIcon();
        header.clickOnTheBrokenHeartButton();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be empty");
    }
}
