package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Epic("Login")
@Severity(SeverityLevel.CRITICAL)
@Feature("User can login to DemoShop App.")
public class DemoShopCartTest {
    Page page = new Page();
    Header header = new Header();
    ProductCards productList = new ProductCards();
    CartPage cartPage = new CartPage();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickToReset();
    }

    @Test(description = "a guest user can go to cart page")
    public void user_as_a_guest_can_navigate_to_cart_page() {
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart Page");
    }

    @Test(description = "user click on Awesome Granite Chips")
    public void user_can_click_on_the_first_product_in_list() {
        productList.clickOnTheFirstProductTitle();
        assertEquals(page.getPageTitle(), "Awesome Granite Chips");
    }

    @Test(description = "user click on Awesome Granite Chips")
    public void user_can_click_on_the_last_product_in_list() {
        productList.clickOnTheLastProductTitle();
        assertEquals(page.getPageTitle(), "Refined Frozen Mouse");
    }

    @Test(description = "user dino can go to cart page")
    public void user_dino_can_navigate_to_cart_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart Page");
    }

    @Test(description = "user turtle can go to cart page")
    public void user_turtle_can_navigate_to_cart_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart Page");
    }

    @Test(description = "user beetle can go to cart page")
    public void user_beetle_can_navigate_to_cart_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart Page");
    }

    @Test(dependsOnMethods = "user_as_a_guest_can_navigate_to_cart_page")
    public void user_as_a_guest_can_navigate_to_homepage_from_cart_page() {
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be Products page");
    }

    @Test(description = "user dino can go to homepage from cart page")
    public void user_dino_can_navigate_to_homepage_from_cart_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be Products page");
    }

    @Test(description = "user turtle can go to homepage from cart page")
    public void user_turtle_can_navigate_to_homepage_from_cart_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be Products page");
    }

    @Test(description = "user beetle can go to homepage from cart page")
    public void user_beetle_can_navigate_to_homepage_from_cart_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be Products page");
    }

    @Test(description = "user dino can add products to cart from product cards")
    public void user_dino_can_add_Product_to_cart_from_product_cards() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }

    @Test(description = "user turtle can add products to cart from product cards")
    public void user_turtle_can_add_Product_to_cart_from_product_cards() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }

    @Test(description = "user beetle cant add product to cart from product cards")
    public void user_beetle_cant_add_Product_to_cart_from_product_cards() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        assertFalse(header.isShoppingBadgeVisible(), "0");
    }

    @Test(description = "user beetle can add products to cart from product cards")
    public void user_beetle_can_add_Product_to_cart_from_product_cards() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }

    @Test(description = "user as a guest can add products to cart from product cards")
    public void user_as_a_guest_can_add_Product_to_cart_from_product_cards() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }

    @Test(description = "user as a guest can add two products to cart from product cards")
    public void user_can_add_two_Product_to_cart_from_product_cards() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two product to cart, badge shows 2.");
    }

    @Test(description = "user as a guest can add three products to cart from product cards")
    public void user_can_add_three_Product_to_cart_from_product_cards() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "3", "After adding three product to cart, badge shows 3.");
    }
}