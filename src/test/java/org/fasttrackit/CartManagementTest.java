package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
//@Ignore
public class CartManagementTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }

    @Test(description = "when user go to cart page empty cart message is shown")
    public void when_user_navigates_to_cart_page_empty_cart_page_message_is_displayed() {
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test(description = "if user add a product to cart, empty cart message is not shown")
    public void adding_one_product_to_cart_empty_cart_message_is_not_shown() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());
    }

    @Test(description = "user can increase the amount of products in cart page")
    public void user_can_increment_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.increaseAmount();
        assertEquals(item.getItemAmount(), "2");
    }

    @Test(description = "user can reduce the amount of products in cart page")
    public void user_can_reduce_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.reduceAmount();
        assertEquals(item.getItemAmount(), "1");
    }

    @Test(description = "user can remove a product from cart")
    public void user_can_delete_a_product_from_the_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheDeleteButton.click();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test(description = "if user click on continue, shopping home page is shown")
    public void when_user_click_on_continue_shopping_home_page_is_displayed() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnContinueShopping.click();
        assertEquals(page.getPageTitle(), "Products", "Expected to be Products page");
    }

    @Test(description = "if user click on checkout button, your information is shown")
    public void when_user_click_on_checkout_button_your_information_is_displayed() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnCheckoutButton.click();
        assertEquals(page.getPageTitle(), "Your information", "Expected to be Your information page");
    }

    @Test(description = "if user click on continue checkout with address information, order summary is shown")
    public void when_user_click_on_continue_checkout_with_address_information_order_summary_is_displayed() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnCheckoutButton.click();
        cartPage.typeInFirstName("Valentin");
        cartPage.typeInLastName("Fazakas");
        cartPage.typeInAddress("Cluj");
        cartPage.clickOnContinueCheckoutButton.click();
        assertEquals(page.getPageTitle(), "Order summary", "Expected to be Order summary page");
    }

    @Test(description = "if user click on continue checkout without completing address information error message is visible")
    public void if_user_click_on_continue_checkout_without_address_information_error_message_is_visible() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnCheckoutButton.click();
        cartPage.clickOnContinueCheckoutButton.click();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        assertTrue(errorMessageVisible, " The user must complete the address information fields.");
    }

    @Test(description = "if user click on complete your order, order complete is shown")
    public void when_user_click_on_complete_your_order_order_complete_is_displayed() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnCheckoutButton.click();
        cartPage.typeInFirstName("Valentin");
        cartPage.typeInLastName("Fazakas");
        cartPage.typeInAddress("Cluj");
        cartPage.clickOnContinueCheckoutButton.click();
        cartPage.clickOnCompleteYourOrder.click();
        assertEquals(page.getPageTitle(), "Order complete", "Expected to be Order complete page");
    }

    @Test(description = "if user click on cancel button, your cart is shown")
    public void when_user_click_on_cancel_button_your_cart_is_displayed() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnCheckoutButton.click();
        cartPage.clickOnCancelButton.click();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be your cart page");
    }
}
