package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
@Test(groups = "AuthenticationTest")
public class AuthenticationTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        footer.clickToReset();
    }

    @Test(description = "User dino can login with valid credentials.")
    public void user_dino_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi dino1!", "Logged in with dino, expected greeting message to be Hi dino!");
    }

    @Test(description = "User dino can logout.")
    public void user_dino_can_logout() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        modal.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Logged out with dino, expected greeting message to be Hello guest!");
    }

    @Test(description = "User dino cant login with incorrect password.")
    public void user_dino_cant_login_with_incorrect_password() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choo");
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        assertTrue(errorMessageVisible, "Error message is shown when invalid credentials used for login");
    }

    @Test(description = "User dino cant login with incorrect username.")
    public void user_dino_cant_login_with_incorrect_username() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino1");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        assertTrue(errorMessageVisible, "Error message is shown when invalid credentials used for login");
    }

    @Test(description = "User turtle can login with valid credentials.")
    @Description("User turtle can login with valid credentials")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Valentin")
    @Link(name = "FastTrackIt demo shop", url = "https://fasttrackit-test.netlify.app")
    @Issue("DMS-001")
    @TmsLink("https://fasttrackit-test.netlify.app/#/issues/DMS-001")
    @Story("Login with valid credentials")
    public void user_turtle_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi turtle!", "Logged in with turtle, expected greeting message to be Hi turtle!");
    }

    @Test(description = "User turtle can logout.")
    public void user_turtle_can_logout() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        modal.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Logged in with turtle, expected greeting message to be Hello guest!");
    }

    @Test(description = "User turtle cant login with incorrect password.")
    public void user_turtle_cant_login_with_incorrect_password() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choo");
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        assertTrue(errorMessageVisible, "Error message is shown when invalid credentials used for login");
    }

    @Test(description = "User turtle cant login with incorrect password.")
    public void user_turtle_cant_login_with_incorrect_username() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle1");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        assertTrue(errorMessageVisible, "Error message is shown when invalid credentials used for login");
    }

    @Test(description = "User beetle can login with valid credentials.")
    public void user_beetle_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi beetle!", "Logged in with beetle, expected greeting message to be Hi beetle!");
    }

    @Test(description = "User turtle can logout.")
    public void user_beetle_can_logout() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        modal.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Logged in with beetle, expected greeting message to be Hello guest!");
    }

    @Test(description = "User beetle cant login with incorrect password.")
    public void user_beetle_cant_login_with_incorrect_password() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choo");
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        assertTrue(errorMessageVisible, "Error message is shown when invalid credentials used for login");
    }

    @Test(description = "User beetle cant login with incorrect username.")
    public void user_beetle_cant_login_with_incorrect_username() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle1");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        assertTrue(errorMessageVisible, "Error message is shown when invalid credentials used for login");
    }

    @Test(description = "User locked cant login with invalid credentials.")
    public void locked_non_functional_authentication() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("locked");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        assertTrue(errorMessageVisible, " The user has been locked out message is shown.");
    }

    @Test(description = "User can navigate as a guest.")
    public void user_can_navigate_as_a_guest() {
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "expected greeting message to be Hello guest!");
    }
}
